# testCU

AdonisJs 4.1 (https://adonisjs.com)  
Vue (https://vuejs.org/)
Vue-Router (https://router.vuejs.org)
Vuex (https://vuex.vuejs.org)

## Требования

1. Node / Npm
2. MySQL

## Установка

```js
cp .env.example .env
```
Настраиваем доступ к БД в .env

```js
npm install
```

```js
node ace migration:run
```

##Запуск
```js
npm run build
npm run start
```

