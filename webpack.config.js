const path = require('path');
const {VueLoaderPlugin} = require('vue-loader');

module.exports = {
  entry: {
    app: './resources/assets/js/app'
  },
  output: {
    filename: "[name].js",
    path: path.resolve(__dirname, 'public')
  },
  devServer: {
    historyApiFallback: true,
    noInfo: true,
    proxy: [
      {
        context: ['**'],
        target: 'http://localhost:3333'
      }
    ]
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader'
        }
      },
      {
        test: /\.s[a|c]ss$/,
        loader: 'style-loader!css-loader!sass-loader'
      },
      {
        test: /\.vue$/,
        loader: 'vue-loader',
        options: {
          extractCSS: true
        }
      }
    ]
  },
  resolve: {
    alias: {
      'vue$': 'vue/dist/vue.esm.js'
    },
    extensions: ['*', '.js', '.vue', '.json']
  },
  plugins: [
      new VueLoaderPlugin()
  ]
};