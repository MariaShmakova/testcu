const Event = use('Event')
const Datum = use('App/Models/Datum');

Event.on('generateAndSendNewData', async () => {
  console.log('generateAndSendNewData start');
  let Data = new Datum();
  Data.send();
  console.log('generateAndSendNewData finish');
});