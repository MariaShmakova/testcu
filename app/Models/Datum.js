'use strict';

const Ws = use('Ws');

class Datum {
  constructor() {
    this.data = {
      rss: Number((process.memoryUsage().rss / 1024 / 1024).toFixed(2)),
      heapTotal: Number((process.memoryUsage().heapTotal / 1024 / 1024).toFixed(2)),
      heapUsed: Number((process.memoryUsage().heapUsed / 1024 / 1024).toFixed(2))
    }
  }

  send () {
    const test = Ws.getChannel('test').topic('test');
    if (test) {
      test.broadcast('message', this.data);
    }
  }
}

module.exports = Datum;
