'use strict';


class UserController {
  async checkAuth({auth, response}) {
    let userIsAuth = true;
    try {
      await auth.check();
    } catch (e) {
      userIsAuth = false;
    }

    return response.send({
      'auth': userIsAuth
    });
  }
}

module.exports = UserController;
