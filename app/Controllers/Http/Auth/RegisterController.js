'use strict';

const User = use('App/Models/User');
const {validate} = use('Validator');

class RegisterController {

  async register({request, response}) {
    const rules = {
      email: 'required|email|unique:users,email',
      password: 'required|min:6'
    };

    const validation = await validate(request.all(), rules);

    if (validation.fails()) {
      return response
          .status(404)
          .send({
            'errors': validation.messages(),
            'code': 404
          });
    }

    const user = new User();

    user.username = request.input('email');
    user.email = request.input('email');
    user.password = request.input('password');

    await user.save();

    return response.redirect('/login');
  }
}

module.exports = RegisterController;
