'use strict';

const User = use('App/Models/User');
const {validate} = use('Validator');


class LoginController {

  async logout({request, auth}) {
    try {
      await auth.authenticator('jwt').revokeTokens();
    } catch (e) {
      console.log(e.message);
    }
  }

  async login({request, response, auth}) {

    // validate
    const rules = {
      email: 'required|email',
      password: 'required'
    };

    const validation = await validate(request.all(), rules);

    if (validation.fails()) {
      return response
          .status(404)
          .send({
            'errors': validation.messages(),
            'code': 404
          });
    }

    const {email, password} = request.all();
    try {
      let token = await auth
          .withRefreshToken()
          .attempt(email, password);

      const userAuth = await User.query().where('email', email).first();

      return response
          .status(200)
          .json({
            user: userAuth,
            auth: token,
            message: 'Login successfully',
            code: 200
          });
    } catch (e) {
      return response.status(e.status).send({
        'code': e.status,
        'errors': [{
          'field': 'email',
          'message': 'Неверная пара логин/пароль'
        }]
      });
    }
  }
}

module.exports = LoginController
