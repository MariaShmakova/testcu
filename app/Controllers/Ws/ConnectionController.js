'use strict';
const Event = use('Event')


let socket_count = 0;

let generateFlag = false;

class ConnectionController {
  constructor({socket, request}) {
    this.socket = socket;
    this.request = request;
    socket_count++;
    if (!generateFlag) this.generate();
  }

  onClose() {
    socket_count--;
  }

  /**
   * Кол-во подключений
   */
  onConnections() {
    this.socket.emit('connections', socket_count)
  }

  generate() {
    let rand = Math.random() * 10 * 1000;
    if (generateFlag = socket_count > 0) {
      setTimeout(() => {
        Event.fire('generateAndSendNewData');
        this.generate();
      }, rand);
    }
  }
}

module.exports = ConnectionController;
