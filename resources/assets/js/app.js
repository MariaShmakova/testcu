import "babel-polyfill"
import Vue from 'vue';
import router from './router';
import store from './store/store';
require('materialize-css');
import '../sass/app.scss';
import Ws from '@adonisjs/websocket-client';

const axios = require('axios');
import api from './api';
import Event from './Event';

window.Event = new Event();
window.api = api;
window.axios = axios;

Vue.component('c-header', require('./components/header.vue').default);
Vue.component('c-footer', require('./components/footer.vue').default);

const wsVuePlugin = function(Vue, url, options) {
  Vue.prototype.$io = Ws(url, options);
};

Vue.use(wsVuePlugin, 'ws://localhost:3333', {});

const app = new Vue({
  el: '#app',
  router,
  store,
  data() {
    return {
      user: this.$store.state.user
    }
  },
  watch: {
    '$store.state.user.token': setToken
  }
});


function setToken (token) {
  axios.defaults.headers.common['Authorization'] = token ? `Bearer ${token}` : '';
}

if (app.$store.state.user && app.$store.state.user.token) setToken(app.$store.state.user.token);