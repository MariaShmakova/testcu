import api from '../../api';
import router from '../../router';

let user = {
  id: null,
  email: null,
  token: null,
  refreshToken: null,
  type: null
};

const getters = {};
const mutations = {
  RESET_DATA_USER(state) {
    state.id = null;
    state.email = null;
    state.token = null;
    state.refreshToken = null;
    state.type = null;
  },
  SET_DATA_USER(state, data) {
    state.id = data.user.id;
    state.email = data.user.email;
    state.token = data.auth.token;
    state.refreshToken = data.auth.refreshToken;
    state.type = data.auth.type;
  }
};

const actions = {
  userRegister({commit, dispatch}, data) {
    return api.v1.auth.registration(data.email, data.password)
        .then(r => {
          window.location.href = '/login';
          return r;
        })
        .catch(e => {return e;})
  },
  userLogin({commit, dispatch}, data) {
    return api.v1.auth.login(data.email, data.password)
        .then(r => {
          commit('SET_DATA_USER', r.data);
          return r;
        })
        .catch(e => {
          return e;
        })
  },
  logoutUser({commit}) {
    api.v1.auth.logout()
        .then(r => {
          commit('RESET_DATA_USER');
          router.push({name: 'login'});
        })
  }
};

export default {
  state: user,
  getters,
  mutations,
  actions
};