let api = {
  v1: {
    auth: {
      login: (email, password, remember) => axios.post('/login', {email, password, remember}),
      logout: () => axios.post('/logout'),
      registration: (email, password) => axios.post('/register', {email, password}),
    }
  }
};

export default api;