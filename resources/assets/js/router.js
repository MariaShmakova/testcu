import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);


const main = Vue.component('page-main', require('./page/main').default);
const register = Vue.component('page-register', require('./page/register').default);
const login = Vue.component('page-login', require('./page/login').default);

const routes = [
  {path: '/', component: main, name: 'main',  meta: {requiresAuth: true}},
  {path: '/register', component: register, name: 'register'},
  {path: '/login', component: login, name: 'login'}
];

const router = new Router({
  mode: 'history',
  routes
});

router.beforeEach((to, from, next) => {
  const requiresAuth = to.matched.some(record => record.meta.requiresAuth);
  if (!requiresAuth) return next();

  axios.get('/auth')
      .then((res) => {
        if (res.data && res.data.auth) return next();
        else {
          router.push({name: 'login'});
        }
      })
      .catch(e => {
        router.push({name: 'login'});
      })
});


export {routes};

export default router;