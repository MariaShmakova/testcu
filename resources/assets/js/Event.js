import Vue from 'vue';

class Event {
  constructor() {
    this.vue = new Vue();
  }
  fire(event, data) {
    this.vue.$emit(event, data);
  }
  listen(event, clb) {
    this.vue.$on(event, clb)
  }
}

export default Event;